import setuptools

setuptools.setup(
    name="vivisect", # Replace with your own username
    version="2019.12.11",
    author="Jonathan",
    author_email="jonathan@pocydon.com",
    description="static analysis / emulation / symbolik analysis framework",
    long_description="Fairly un-documented static analysis / emulation / symbolik analysis framework for PE/Elf/Mach-O/Blob binary formats on various architectures.",
    long_description_content_type="text/markdown",
    url="https://gitlab.com/twosix-kali/packages1/vivisect.git",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 2",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=2.7',
)

